#pragma once
#include <utility>
#include <vector>
#include "KdTree.h"

class MedianOfMedians
{
public:
	static int getMedianOfMedians(Feature points[], int numberOfElements, int k);

private:

	// All functions relating to linearTimeSelection
	// AKA Median Finding Algorithm or Median of Medians,
	// used for finding the axis median in O(n) time worst case
	static int linearTimeSelection(Feature points[], int numberOfElements, int k);
	static Feature findMedian(std::vector<Feature> subsets);

};