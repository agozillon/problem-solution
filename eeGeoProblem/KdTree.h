#pragma once
#include <memory>
#include <string>

struct Feature
{
	Feature(){
		m_dimension[0] = 0; m_dimension[1] = 0;
	}

	std::string m_name;
	int m_dimension[2];
};

class KdNode
{
	friend class KdTree; // allows KD tree to access what it wants without the need for get/set functions or allowing public access

	public:
		KdNode(){};
		~KdNode(){};

	protected:
		Feature m_feature;
		std::shared_ptr<KdNode> m_left, m_right;
};

// Basic KD Tree it's a little unflexible in the sense that it doesn't deal with any dimensionallity other than 2D for the moment
// It also only has a tiny amount of the functionallity a normal KD Tree could/would have i.e. just Nearest Neighbour search.
class KdTree
{
	public:
		KdTree(Feature points[], int numberOfElements);
		~KdTree(){}; // shared pointers are used for the tree, deallocation not explicitly required
		Feature findNearestNeighbour(Feature searchedFeature, double& bestDistance);

	private:
		std::shared_ptr<KdNode> generateTree(Feature points[], int depth, int start, int end);
		void findNearest(std::shared_ptr<KdNode> currentNode, Feature searchedFeature, Feature& nearest, double& bestDist, int depth);
		std::shared_ptr<KdNode> m_root;

};

