#pragma once
#include "KdTree.h"

// Quick Sort algorithm that at the moment only sorts Features (by whatever axis we wish it to be sorted), should make it more generic in the future.
class QuickSort{

public:
	static void quickSort(Feature points[], int first, int last, int axis);

private:
	static int partition(Feature points[], int first, int last, int axis);

};