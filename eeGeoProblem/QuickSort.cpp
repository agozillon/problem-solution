#include "QuickSort.h"

// Quick sort recursive function made static so it doesn't require instantiation.
// @ Param Feature[] points - the feature data that you wish sorted 
// @ Param int first - the area to start parsing the data set
// @ Param int last - the area to stop parsing the data set
// @ Param int axis - the axis you want to sort, either x(0) or y(1) 
void QuickSort::quickSort(Feature points[], int first, int last, int axis)
{
	int pivot;

	if (first < last) // if there is remaining points
	{
		pivot = partition(points, first, last, axis); // calculate the pivot to split the data down for further sorting (also sorts the data)
		quickSort(points, first, pivot - 1, axis); // take the first half of the remaining points and recalls itself
		quickSort(points, pivot + 1, last, axis); // take the final half of the remaining points and recalls itself

	}


}

// Function that sorts the data and splits it into two, only deals with integer Feature data at the moment
// It's a Lomuto parition scheme based quick sort 
// @ Param Feature[] points - data set to sort
// @ Param int first - the index to start parsing the data at
// @ Param int last - the inex to stop parsing the data at
// @ Param int axis - the axis we're currently sorting, either x:0 or y:1
// @ Param int pivot - returns the pivot index, where we wish to split the data 
int QuickSort::partition(Feature points[], int first, int last, int axis)
{
	Feature temp, pivotElement = points[last];
	int pivot = first;
	
	for (int i = first; i < last; ++i)
	{
		if (points[i].m_dimension[axis] <= pivotElement.m_dimension[axis])
		{
			// swap the data in the pivot position and currently checked position around
			temp = points[pivot];
			points[pivot] = points[i];
			points[i] = temp;

			++pivot; // increment pivot position
		}
	}

	// swap the data in the pivot position and the final end spot around
	temp = points[pivot];
	points[pivot] = points[last];
	points[last] = temp;
	
	return pivot;
}