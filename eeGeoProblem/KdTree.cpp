#include "KdTree.h"

#include <limits>

#include "QuickSort.h"

// constructor that generates the tree from passed in data using a private recursive function.
// @Param Feature[] points - the feature data that we wish to add to the tree
// @Param int numberOfElements - the number of elements in the data set
KdTree::KdTree(Feature points[], int numberOfElements)
{
	// if theres any elements then generate the tree from the exisiting data
	if (numberOfElements > 0)
	{
		m_root = generateTree(points, 0, 0, numberOfElements-1);
	}
}

// Recursive function that generates the tree node structure of the kd tree, runs at O(n log^2 n) complexity (Quicksort + Divide and Conquer Recursion) 
// at worst quick sort can be higher, but in this case it should never reach worst case complexity as the data is always unsorted as we 
// constantly sort by the two different axises.
// @Param Feature[] points - array of feature data (2d co-ords with a name)
// @Param int depth - the depth in the tree the recursion function currently is, mostly to check what axis we should be partitioning
// @Param int start - start of the data set (so we can partition the array as we reccur further down)
// @Param int end - similar to start, just indicates the end of the area the function is interested in
// @Return std::shared_ptr<KdNode> - the KdNode created from the call to generateTree, can be NULL if there is no data left
std::shared_ptr<KdNode> KdTree::generateTree(Feature points[], int depth, int start, int end)
{	
	// get median by quick sorting the data and picking out the central value
	int axis = depth % 2; // depth divided by number of dimensions 2 to decide on which axis we need to find the median on
	QuickSort::quickSort(points, start, end - 1, axis);
	int median = (end - start) / 2;

	// create current node from median and fill with data
	std::shared_ptr<KdNode> node = std::shared_ptr<KdNode>(new KdNode());
	
	// check if there's any data left, if there is reccur deeper for another node, otherwise return null 
	if (start < end)
	{
		node->m_left = generateTree(points, depth + 1, start, (start + median) - 1); // split data down the left side of the median and pass it down
		node->m_right = generateTree(points, depth + 1, (start + median) + 1, end); // split data down the right side of the median and pass it down
	}

	// if theres still data left in the array for this current reccurence then create a node and return it, else return null
	if (start <= end)
	{
		node->m_feature.m_dimension[0] = points[start + median].m_dimension[0];
		node->m_feature.m_dimension[1] = points[start + median].m_dimension[1];
		node->m_feature.m_name = points[start + median].m_name;
	}
	else
		node = NULL;
	
	return node;
}

// baiscally just abstracting the recusrive function away to simplify the interface for anyone using it
// don't want them putting in incorrect data to the recursive function. 
// @Param Feature searchedFeature - basically just the Feature you wish to finds nearest neighbour
// @Param double& bestDistance - returns the distance from the nearest neighbour to the searchedFeature 
// @Return Feature - returns the nearest neighbour to the searched feature
Feature KdTree::findNearestNeighbour(Feature searchedFeature, double& bestDistance)
{
	Feature tmp;
	// not neccessarily the ideal solution but it works, basically sets it to the max value for its type
	// as if it started at 0 then nothing could ever be closer/the best distance
	double tmpBest = std::numeric_limits<double>::max(); 
	findNearest(m_root, searchedFeature, tmp, tmpBest, 0);  
	bestDistance = tmpBest;
	return tmp;
}

// The findNearest recursive function, it runs at O(n log n) complexity, by acting pretty much like a normal binary search on an integer
// except checking the distance the searched for feature is away from the current axis split (the node value) and then going down left or right
// @Param std::shared_ptr<KdNode> currentNode - the node that we're currently rooted at and checking 
// @Param Feature searchedFeature - the Feature that we're looking fors nearest neighbour (can be an arbitrary feature or one from the data set)
// @Param Feature& nearest - the current nearest neighbour, once it's found it'll get returned up the call stack to the original call
// @Param double& bestDist - the current distance to the closest neighbour, of big note is that it returns the squared euclidian distance, so you must square root it to get 
//							 the normal euclidian distance. The reason the function uses the squared euclidian is that its less computationally expensive.						
// @Param int depth - the current depth we're at in the tree, used to calculate what axis we should be looking at
void KdTree::findNearest(std::shared_ptr<KdNode> currentNode, Feature searchedFeature, Feature& nearest, double& bestDist, int depth)
{
	if (currentNode == NULL) // have we reached beyond a leaf node, if so return
		return;

	// basically just checking the distance between the feature and the current node on the current axis (we swap axis each depth level)
	// if its positive then its to the right of the current node/axis split, if its less than then its to the left of the current node split 
	int axisCheck = depth % 2;
	++depth;

	// calculate the squared euclidian distance for the two co-ordinates
	// calculate the distance between the current axis co-ordinates, i.e. if on depth level 0, then the partition is on the x axis, so look at the x axis values
	// calculate the squared euclidian distance for the one axis
	// The idea to use squared euclidian distance over normal euclidian distance came from looking around at a couple of examples and realizing that its quicker than
	// euclidian distance due to the square root, thus is used it in the function...no sense wasting a good idea!
	double squaredEuclid = std::pow((currentNode->m_feature.m_dimension[0] - searchedFeature.m_dimension[0]), 2) + std::pow((currentNode->m_feature.m_dimension[1] - searchedFeature.m_dimension[1]), 2);
	double axisDistance = currentNode->m_feature.m_dimension[axisCheck] - searchedFeature.m_dimension[axisCheck];
	double axisEuclid = std::pow(axisDistance, 2);

	
	// if the distance to this node is better than the current best
	// and it's not the exact same feature (based on name in this case, could maybe dissalow
	// it via positions but I assume features can overlap but have different identifying names in this case)
	// replace the current best distance with the newest best and copy the nearest feature in.
	if (bestDist > squaredEuclid 
		&& searchedFeature.m_name != currentNode->m_feature.m_name)
	{
		nearest = currentNode->m_feature;
		bestDist = squaredEuclid;
	}
	
	// if the distance from the searched feature to the current feature is greater than 0 go down the left side 
	// as if the searched feature is smaller on the current axis then the dx value will be positive e.g. (5 - 2) else go down the right
	// as a feature larger than the current axis value will be negative  (5 - 10) 
	if (axisDistance > 0)
		findNearest(currentNode->m_left, searchedFeature, nearest, bestDist, depth);
	else
		findNearest(currentNode->m_right, searchedFeature, nearest, bestDist, depth);
	
	// basically after going down that side of the tree
	// is the new best distance retrieved from there smaller than the current axis
	// squared euclidian if it is then theres no point going down the other 
	// parition as its on the opposite side and would be larger anyway (as it'd be going further away from the 
	// searched feature) 
	if (axisEuclid >= bestDist) 
		return;
	
	// just swap the above around to get it to go down the next side
	// if it hasnt't exited yet
	if (axisDistance > 0)
		findNearest(currentNode->m_right, searchedFeature, nearest, bestDist, depth);
	else
		findNearest(currentNode->m_left, searchedFeature, nearest, bestDist, depth);

}