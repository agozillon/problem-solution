#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <math.h>
#include <memory>
#include <ctime>

#include "KdTree.h"
#include "QuickSort.h"



// Basic file output function - really only used to output the answer
// @Param const char* fileName - filename for the file/filepath you wish to output to, will
//								 create a file if its not existant
// @Param const char* outputData - the data you wish output to file
// @Param int sizeOfArray - the size of the character array you wish to output to file
void outputFile(const char* fileName, const char* outputData, int sizeOfArray)
{
	// open output file stream
	std::ofstream writeStream;
	writeStream.open(fileName, std::ofstream::out);

	// check its open and all went well then start writing the data to file
	if (writeStream.is_open())
	{
		writeStream.write(outputData, sizeOfArray);
	}
}

// Basic file loading function
// @Param const char* fileName - file name for the file you wish to be loaded in
// @Param Feature array - array to pass in that'll be filled with the values loaded in 
// @Param int& sizeOfArray - the size of the array loaded in will be copied into this 
void loadFile(const char* fileName, Feature array[], int& sizeOfArray)
{
	std::ifstream loadStream;
	loadStream.open(fileName, std::ifstream::in);

	std::string tmp;
	std::stringstream istream;
	int i = 0;
	
	if (loadStream.is_open())
	{
		// read line from file, so feature name, x and y, interestingly when a 
		// stream is used for a logical operation like this (getline returns an istream)
		// it works as an eof(), it'll return false on end of file and true everywhere else!
		// And means no need for any extra checks to catch that final empty value having an eof in it's
		// place would require. 
		while (std::getline(loadStream, tmp))
		{
			// place line in a stream so it can be used with more getlines
			// (NOTE:SELF unsure if this is faster than a char by char check, worth a look up
			// and change if spare time)
			istream << tmp; 

			// feature name
			std::getline(istream, tmp, ' ');
			array[i].m_name = tmp;

			// read features X
			std::getline(istream, tmp, ' ');
			array[i].m_dimension[0] = std::atoi(tmp.c_str());
			
			// read features Y 
			std::getline(istream, tmp, ' ');
			array[i].m_dimension[1] = std::atoi(tmp.c_str());

			istream.clear();

			++i;
		}
	}

	sizeOfArray = i;
}

// Just to partition the problem and not have a huge copy pasted main...
std::string runTest(const char* fileName)
{
	std::cout << "Start of Test" << std::endl;

	clock_t before, after;

	// document says up to 100000...but the big problem file has 100001
	// (If I was to further tweak the code I'd base most things around a vector container
	// instead of an array as it'd give more flexibility)
	Feature* features = new Feature[100001];
	int featureListSize = 0;

	// load in the feature data from file
	before = clock();
	loadFile(fileName, features, featureListSize);
	after = clock();
	std::cout << "Load Time: " << (double)(after - before) / CLOCKS_PER_SEC << std::endl;

	// fill the kd tree with the loaded in data, the complexity of this is 
	// O(n log^2 n), as I use a quicksort O(n log n) to find the median, on top of the
	// O(log n) that a binary tree/kd-tree uses normally. It can be modified to 
	// O(n) complexity if a median of medians algorithm is used, or less if you take
	// arbitrary medians (at the cost of much slower searches due to an unbalanced tree). 
	before = clock();
	std::unique_ptr<KdTree> kdTree = std::unique_ptr<KdTree>(new KdTree(features, featureListSize));
	after = clock();
	std::cout << "Tree Build Time: " << (double)(after - before) / CLOCKS_PER_SEC << std::endl;

	Feature currentBest;
	double bestDist = 0, newDist = 0;

	before = clock();

	// A for loop that uses a kD tree to find the nearest neighbour for each element
	// which is then compared to find the highest nearest neighbour (or furthest nearest neighbour)
	// approximately O(n log n) complexity, log n for the findNearestNeighbour (in a balanced kd-tree so as efficent as it can be)
	// plus n for the linear loop 
	for (int i = 0; i < featureListSize; ++i)
	{
		kdTree->findNearestNeighbour(features[i], newDist); // find the current elements nearest neigbour

		// check if its distance is greater than the best distance (i.e. its nearest neighbour is further than the previous nearest neighbour)
		if (newDist > bestDist) 
		{
			bestDist = newDist;
			currentBest = features[i];
		}
	}

	after = clock();

	std::cout << "Time taken to find: " << (double)(after - before) / CLOCKS_PER_SEC << std::endl;
	std::cout << "Found Most Isolated Point: " << currentBest.m_name << std::endl;
	std::cout << "End of Test" << std::endl;
	std::cout << "Deleting Data" << std::endl << std::endl;

	delete[] features; 

	return currentBest.m_name;
}

int main()
{	
	std::string outputData;

	// run small test
	outputData = "Small Problem Answer: ";
	outputData += runTest("Resources/Map Files/problem_small.txt") + "\n";
 
	// run big test
	outputData += "Big Problem Answer: ";
	outputData += runTest("Resources/Map Files/problem_big.txt");
	 
	std::cout << "Outputting Answers to File ProblemAnswer.txt" << std::endl;

	// output answers to file
	outputFile("ProblemAnswer.txt", outputData.c_str(), outputData.size());

	return 0;
}


