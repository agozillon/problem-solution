#include "MedianOfMedians.h"
#include "QuickSort.h"
#include <math.h>
#include <vector>
#include <iostream>

int MedianOfMedians::getMedianOfMedians(Feature points[], int numberOfElements, int k)
{
	if (numberOfElements < 10) // element set is small enough to just quicksort it and take the middle element
	{
		QuickSort::quickSort(points, 0, numberOfElements - 1, 0);
		return points[numberOfElements / 2].m_dimension[0]; // return the median x axis value
	}
	else
	{
		return linearTimeSelection(points, numberOfElements, k);
	}
}

int MedianOfMedians::linearTimeSelection(Feature points[], int numberOfElements, int k)
{
	// split data set into subsets of 5, using vectors to make things less messy don't want hanging pointers.
	int subsetCount = std::ceil((float)numberOfElements / 5);

	std::vector<std::vector<Feature>> subsets(subsetCount, std::vector<Feature>(5));

	int i = 0, x = 0, y = 0;
	while (i < numberOfElements)
	{
		subsets[x][y] = points[i];
		++y;

		if (y > 4)
		{
			y = 0;
			++x;
		}
		++i;
	}



	// find median of medians (get median for all subsets, then get median from the found medians)
	i = 0;
	std::vector<Feature> medians(subsetCount);
	while (i < subsetCount)
	{
		medians[i] = findMedian(subsets[i]);
		++i;
	}

	
	Feature m = findMedian(medians);

	std::vector<Feature> L1, L2;
	i = 0;

	for (; i < subsetCount; ++i)
	{
		for (int j = 0; j < 5; ++j)
		{
			if (subsets[i][j].m_dimension[0] > m.m_dimension[0])
			{
				L1.push_back(subsets[i][j]);
			}
			else if (subsets[i][j].m_dimension[0] < m.m_dimension[0])
			{
				L2.push_back(subsets[i][j]);
			}
		}
	}

	// K can actually be picked to not be the median of a set of numbers, as the algorithm can
	// technically be extended to find any K'th element in an set of N
	if ((k - 1) == L1.size())
	{
		return linearTimeSelection(L1.data(), L1.size(), k);
	}
	else if (k <= L1.size())
	{
		return m.m_dimension[0];
	}
	else if (k > (L1.size() + 1))
	{
		return linearTimeSelection(L2.data(), L2.size(), k - ((int)L1.size()) - 1);
	}
}

Feature MedianOfMedians::findMedian(std::vector<Feature> subsets)
{
	QuickSort::quickSort(subsets.data(), 0, subsets.size() - 1, 0); // sort data and return median
	return subsets[std::ceil(subsets.size() / 2)];
}
