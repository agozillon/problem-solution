Problem Solution:

I solved the problem by creating a KD-Tree so that I could run a rapid search to find the nearest neighbour for each feature
the distance between the current feature and its nearest neighbour are then checked against all other nearest neighbour combinations
to find the one with the furthest away nearest neighbour. The feature with the furthest away nearest neighbour is then the most
isolated feature! (as everything else had things closer to it).

The answers are outputed to a file called ProblemAnswer.txt which is in the folder with all the source code, if its deleted it'll recreate the file. If it already exists it'll overwrite it. Other than that timings and the answer are cout printed to screen. 


Isolated Feature Search:

The actual finding of the feature is an O(n log n) complexity calculation. O(n) for a simple for loop checking every features nearest neighbour and checking if the distance is greater than the current largest distance between neighbours. As well as O(log n) for the actual nearest neighbour search which works pretty similar to a standard binary search tree, which a KD Tree is based off of. 

Tree Creation:

The KD Tree creation is O(n log^2 n) complexity, log n for the divide and conquer element that the tree has it essentially being a binary tree and n log n for the quicksort used in its creation (for finding the median value). The complexity can be changed to
O(n log n) if an O(n) algorithm such as a median of medians algorithm is used in quicksorts place. However I did try this and found it was actually slower than just using quick sort for the current level of elements. You can see the FindMediansOfMedians class in the source folder, its not used but i left it there anyway, it's not cleaned or optimized as I dropped it when i found out quick sort worked better. So look at your own peril!

Any issues please contact me at andrew.gozillon@yahoo.com